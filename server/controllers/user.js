var _ = require('underscore'),
    User = require('../routes/').User,
    userRoles = require('../../client/js/routingConfig').userRoles;

module.exports = {
    index: function (req, res) {
        User.findAll(function (err, users) {
            if (err) return res.json(400, err);
            if (!users) return res.json(404);
            res.json(200, users);
        });
    }
};