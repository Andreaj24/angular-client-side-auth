var passport = require('passport'),
    User = require('../routes/').User

module.exports = {
    register: function (req, res, next) {
        try {
            User.validate(req.body);
        } catch (err) {
            console.log("404");
            return res.send(400, err.message);
        }

        User.addUser(req.body.username, req.body.password, req.body.role, function (err, user) {
            if (err === 'UserAlreadyExists') return res.send(403, "User already exists");
            else if (err || !user) return res.send(500);
            req.logIn(user, function (err) {
                if (err) {
                    next(err);
                } else {
                    res.json(200, {
                        "role": user.role,
                        "username": user.username
                    });
                }
            });
        });
    },

    login: function (req, res, next) {
        console.log("LOGIN")
        passport.authenticate('local', function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.send(400);
            }

            req.logIn(user, function (err) {
                if (err) {
                    console.log("Error logIn", err)
                    return next(err);
                }

                if (req.body.rememberme) req.session.cookie.maxAge = 1000 * 60 * 60 * 24 * 7;
                console.log("SESSION CHECk"  , req.user);
                res.json(200, {
                    "role": user.role,
                    "username": user.username
                });
            });
        })(req, res, next);
    },

    logout: function (req, res) {
        req.logout();
        res.send(200);
    }
};