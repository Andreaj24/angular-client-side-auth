var mongoose = require("mongoose"),
    configuration = require('../conf.json'),
    models = require("./models/");

// open shift- connection
//    var dbUrl = "mongodb://" + process.env.OPENSHIFT_MONGODB_DB_USERNAME +
//        ":" + process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
//        process.env.OPENSHIFT_MONGODB_DB_HOST + ":" +
//        process.env.OPENSHIFT_MONGODB_DB_PORT + "/" +

// Db url connection 
var dbUrl = configuration.db.url,
    database = mongoose.createConnection(dbUrl);


database.on('error', function (err) {
    console.error("Connection error: " + dbUrl + "\n" + err);
});
database.once('open', function () {
    console.info("[Database] connection correct");
});

var dbInstances = {
    User: database.model('User', models.User, 'users')
};

exports.db = dbInstances;
// Database Middelware
exports.middleware = function _db(req, res, next) {
    req.db = dbInstances;
    return next();
};