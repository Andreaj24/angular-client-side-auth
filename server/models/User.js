// User model for database
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

exports.User = new Schema({
    id: {
        type: Number,
        unique: false
    },
    username: {
        type: String,
        unique: false
    },
    password: {
        type: String,
        unique: false
    },
    role: {
        bitMask: {
            type: Number
        },
        title: {
            type: String
        }
    }

});