var User, _ = require('underscore'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    GoogleStrategy = require('passport-google').Strategy,
    LinkedInStrategy = require('passport-linkedin').Strategy,
    check = require('validator').check,
    userRoles = require('../../client/js/routingConfig').userRoles,
    _db = require('../database').db;


exports.addUser = function (username, password, role, callback) {

    module.exports.findByUsername(username, function (user) {
        if (user) return callback("UserAlreadyExists", null);
        var user = new _db.User({
            username: username,
            password: password,
            role: role
        });
        user.save(function (err) {
            if (err) {
                console.log("Error ", err);
                if (err.code == "11000") return callback("UserAlreadyExists", null);
                return callback("Error", null);
            }
            return callback(null, user);
        });
    });
};

exports.findOrCreateOauthUser = function (provider, providerId) { // TODO
    var user = module.exports.findByProviderId(provider, providerId);
    if (!user) {
        user = {
            id: Math.random(),
            username: provider + '_user', // Should keep Oauth users anonymous on demo site
            role: userRoles.user,
            provider: provider
        };
        user[provider] = providerId;
        users.push(user);
    }

    return user;
}

exports.findAll = function (finish) {
    console.log("FInd All");

    _db.User.find({}, {
        password: 0
    }).exec(function (err, users) {
        finish(err, users)
    });
};

exports.findById = function (id, finish) {
    _db.User.findOne({
        _id: id
    }).exec(function (err, user) {
        console.log("findById " + user);
        finish(user);
    });
};

exports.findByUsername = function (username, finish) {
    _db.User.findOne({
        username: username
    }).exec(function (err, user) {
        console.log("Find By username ");
        finish(err ? false : user);
    });
};

exports.findByProviderId = function (provider, id) { // TODO

    _db.User.find({
        username: username
    }).exec(function (err, user) {
        return err ? undefined : user
    });

    return _.find(users, function (user) {
        return user[provider] === id;
    });
};

exports.validate = function (user) {
    check(user.username, 'Username must be 1-20 characters long').len(1, 20);
    check(user.password, 'Password must be 5-60 characters long').len(5, 60);
    check(user.username, 'Invalid username').not(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/);

    // TODO: Seems node-validator's isIn function doesn't handle Number arrays very well...
    // Till this is rectified Number arrays must be converted to string arrays
    // https://github.com/chriso/node-validator/issues/185
    var stringArr = _.map(_.values(userRoles), function (val) {
        return val.toString()
    });
    check(user.role, 'Invalid user role given').isIn(stringArr);
};

exports.localStrategy = new LocalStrategy(
    function (username, password, done) {
        module.exports.findByUsername(username, function (user) {

            console.log("FIND USER BY ID FROM LOCALSTRATEGY", user);
            console.log("PAssword", password);
            if (!user) {
                done(null, false, {
                    message: 'Incorrect username.'
                });
            } else if (user.password != password) {
                done(null, false, {
                    message: 'Incorrect username.'
                });
            } else {
                console.log("DONE");
                done(null, user);
            }
        });
    }
);

exports.twitterStrategy = function () {
    if (!process.env.TWITTER_CONSUMER_KEY) throw new Error('A Twitter Consumer Key is required if you want to enable login via Twitter.');
    if (!process.env.TWITTER_CONSUMER_SECRET) throw new Error('A Twitter Consumer Secret is required if you want to enable login via Twitter.');

    return new TwitterStrategy({
            consumerKey: process.env.TWITTER_CONSUMER_KEY,
            consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
            callbackURL: process.env.TWITTER_CALLBACK_URL || 'http://localhost:8000/auth/twitter/callback'
        },
        function (token, tokenSecret, profile, done) {
            var user = module.exports.findOrCreateOauthUser(profile.provider, profile.id);
            done(null, user);
        });
}

exports.facebookStrategy = function () {
    if (!process.env.FACEBOOK_APP_ID) throw new Error('A Facebook App ID is required if you want to enable login via Facebook.');
    if (!process.env.FACEBOOK_APP_SECRET) throw new Error('A Facebook App Secret is required if you want to enable login via Facebook.');

    return new FacebookStrategy({
            clientID: process.env.FACEBOOK_APP_ID,
            clientSecret: process.env.FACEBOOK_APP_SECRET,
            callbackURL: process.env.FACEBOOK_CALLBACK_URL || "http://localhost:8000/auth/facebook/callback"
        },
        function (accessToken, refreshToken, profile, done) {
            var user = module.exports.findOrCreateOauthUser(profile.provider, profile.id);
            done(null, user);
        });
};

exports.googleStrategy = function () {

    return new GoogleStrategy({
            returnURL: process.env.GOOGLE_RETURN_URL || "http://localhost:8000/auth/google/return",
            realm: process.env.GOOGLE_REALM || "http://localhost:8000/"
        },
        function (identifier, profile, done) {
            var user = module.exports.findOrCreateOauthUser('google', identifier);
            done(null, user);
        });
};

exports.linkedInStrategy = function () {
    if (!process.env.LINKED_IN_KEY) throw new Error('A LinkedIn App Key is required if you want to enable login via LinkedIn.');
    if (!process.env.LINKED_IN_SECRET) throw new Error('A LinkedIn App Secret is required if you want to enable login via LinkedIn.');

    return new LinkedInStrategy({
            consumerKey: process.env.LINKED_IN_KEY,
            consumerSecret: process.env.LINKED_IN_SECRET,
            callbackURL: process.env.LINKED_IN_CALLBACK_URL || "http://localhost:8000/auth/linkedin/callback"
        },
        function (token, tokenSecret, profile, done) {
            var user = module.exports.findOrCreateOauthUser('linkedin', profile.id);
            done(null, user);
        }
    );
};

exports.serializeUser = function (user, done) {
    console.log("serializeUser");
    done(null, user._id);
};

exports.deserializeUser = function (id, done) {
    console.log("done", done);
    console.log("deserializeUser ", id);
    module.exports.findById(id, function (user) {
        if (user) {
            console.log("User exist")
            done(null, user);
        } else {
            console.error("User Not  exist")
            done(null, false);
        }
    });
};